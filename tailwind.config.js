/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        'main': '#16ABF8',
        'white': '#FFFFFF',
        'grey': '#888888',
        'black': '#111111',
        'softgrey': '#F4F4F4',
        'boldgrey': '#4A4A4A',
        'red': '#ED4C5C',
        'boldwhite': '#E5E5E5',
        'yellow': '#F8A541',
        'green': '#00A790',
        'blue': '#428BC1',
        'purple': '#8942C1'
      },
    },
  },
  plugins: [],
};
