import React from "react";
import dateFormat from "dateformat";
import { useNavigate } from "react-router-dom";

function ActivityState({
  item: { created_at, title, id },
  getId,
  getTitle,
  modal,
}) {
  const navigate = useNavigate();
  return (
    <div
      onClick={() => {
        navigate(`/detail/${id}`);
      }}
      className="w-[235px] h-[234px] flex bg-white rounded-[12px] py-[22px] px-[27px] flex-col justify-between">
      <p className="text-lg font-bold text-black">{title}</p>
      <div className="flex justify-between items-end">
        <p className="text-grey text-sm font-medium">
          {dateFormat(created_at, "dd mmmm yyyy")}
        </p>
        <svg
          className="cursor-pointer"
          onClick={() => {
            getId(id);
            getTitle(title);
            modal();
          }}
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M4 7H20"
            stroke="#888888"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M10 11V17"
            stroke="#888888"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M14 11V17"
            stroke="#888888"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M5 7L6 19C6 19.5304 6.21071 20.0391 6.58579 20.4142C6.96086 20.7893 7.46957 21 8 21H16C16.5304 21 17.0391 20.7893 17.4142 20.4142C17.7893 20.0391 18 19.5304 18 19L19 7"
            stroke="#888888"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M9 7V4C9 3.73478 9.10536 3.48043 9.29289 3.29289C9.48043 3.10536 9.73478 3 10 3H14C14.2652 3 14.5196 3.10536 14.7071 3.29289C14.8946 3.48043 15 3.73478 15 4V7"
            stroke="#888888"
            stroke-width="2"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      </div>
    </div>
  );
}

export default ActivityState;
