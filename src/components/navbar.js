import React from "react";

function Navbar() {
  return (
    <div className="w-full h-[105px] bg-main px-[220px] flex items-center">
      <p className="text-2xl font-bold text-white">TO DO LIST APP</p>
    </div>
  );
}

export default Navbar;
