import React from "react";
import axios from "axios";
import { useState } from "react";

function ListItem({
  item: { title, priority, is_active, id },
  getId,
  getTitle,
  modal,
  idParent,
}) {
  const [titleList, setTitleList] = useState("");
  const [titleId, setTitleId] = useState("");
  const [updateList, setUpdateList] = useState(false);
  const handleUpdateList = () => {
    axios
      .put(`https://todo.api.devcode.gethired.id/todo-items/${id}`, {
        title: titleList,
        created_at: "2023-04-14 12:00:00",
        updated_at: "2023-04-14 12:00:00",
        activity_group_id: idParent,
        is_active: is_active,
        priority: priority,
      })
      .then(setUpdateList(false))
      .catch((error) => {
        console.log(error);
      });
  };

  const [isChecked, setIsChecked] = useState(is_active === 0);
  const [checkId, setCheckId] = useState("");
  const handleUpdateCheck = () => {
    setIsChecked(!isChecked);
    axios
      .put(`https://todo.api.devcode.gethired.id/todo-items/${checkId}`, {
        title: title,
        created_at: "2023-04-14 12:00:00",
        updated_at: "2023-04-14 12:00:00",
        activity_group_id: idParent,
        is_active: isChecked ? 1 : 0,
        priority: priority,
      })
      // .then(setUpdateList(false))
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div className="mb-[10px] py-[30px] px-[28px] bg-white w-[1000px] h-[80px] rounded-[12px] flex items-center justify-between">
      <div className="flex items-center gap-[16px]">
        {is_active == 1 ? (
          <>
            <input
              id="default-checkbox"
              type="checkbox"
              checked={isChecked}
              onChange={handleUpdateCheck}
              onClick={() => setCheckId(id)}
              class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
            />
            {priority == "very-high" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-red"></div>
            ) : priority == "high" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-yellow"></div>
            ) : priority == "normal" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-green"></div>
            ) : priority == "low" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-blue"></div>
            ) : (
              <div className="w-[9px] h-[9px] rounded-full bg-purple"></div>
            )}

            {updateList === false ? (
              <>
                <label
                  for="default-checkbox"
                  class="text-sm font-medium text-black">
                  {title}
                </label>
              </>
            ) : (
              <>
                <input
                  className="text-sm font-medium text-black border-b border-gray-500 bg-transparent !focus:border-b"
                  type="text"
                  value={titleList}
                  onChange={(e) => {
                    setTitleList(e.target.value);
                    setTitleId(id);
                  }}
                  onBlur={handleUpdateList}
                />
              </>
            )}
          </>
        ) : (
          <>
            <input
              id="default-checkbox"
              type="checkbox"
              checked
              onChange={handleUpdateCheck}
              onClick={() => setCheckId(id)}
              class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
            />
            {priority == "very-high" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-red"></div>
            ) : priority == "high" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-yellow"></div>
            ) : priority == "normal" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-green"></div>
            ) : priority == "low" ? (
              <div className="w-[9px] h-[9px] rounded-full bg-blue"></div>
            ) : (
              <div className="w-[9px] h-[9px] rounded-full bg-purple"></div>
            )}
            <label
              for="default-checkbox"
              class="text-sm font-medium text-blback line-through">
              {title}
            </label>
          </>
        )}
        <svg
          onClick={() => setUpdateList(true)}
          width="20"
          height="21"
          viewBox="0 0 20 21"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <path
            d="M3.3335 17.1665H6.66683L15.4168 8.41654C15.8589 7.97451 16.1072 7.375 16.1072 6.74988C16.1072 6.12475 15.8589 5.52524 15.4168 5.08321C14.9748 4.64118 14.3753 4.39285 13.7502 4.39285C13.125 4.39285 12.5255 4.64118 12.0835 5.08321L3.3335 13.8332V17.1665Z"
            stroke="#C4C4C4"
            stroke-width="1.5"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
          <path
            d="M11.25 5.9166L14.5833 9.24993"
            stroke="#C4C4C4"
            stroke-width="1.5"
            stroke-linecap="round"
            stroke-linejoin="round"
          />
        </svg>
      </div>
      <svg
        onClick={() => {
          getId(id);
          getTitle(title);
          modal();
        }}
        className="cursor-pointer"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <path
          d="M4 7H20"
          stroke="#888888"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M10 11V17"
          stroke="#888888"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M14 11V17"
          stroke="#888888"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M5 7L6 19C6 19.5304 6.21071 20.0391 6.58579 20.4142C6.96086 20.7893 7.46957 21 8 21H16C16.5304 21 17.0391 20.7893 17.4142 20.4142C17.7893 20.0391 18 19.5304 18 19L19 7"
          stroke="#888888"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
        <path
          d="M9 7V4C9 3.73478 9.10536 3.48043 9.29289 3.29289C9.48043 3.10536 9.73478 3 10 3H14C14.2652 3 14.5196 3.10536 14.7071 3.29289C14.8946 3.48043 15 3.73478 15 4V7"
          stroke="#888888"
          stroke-width="2"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
      </svg>
    </div>
  );
}

export default ListItem;
