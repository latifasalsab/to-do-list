import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import Navbar from "../components/navbar";
import EmptyList from "../components/emptyList";
import ListItem from "./listItem";
import PropTypes from "prop-types";
import { styled } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
  "& .css-1t1j96h-MuiPaper-root-MuiDialog-paper": {
    borderRadius: "12px",
    width: "830px",
  },
  "& .MuiDialogContent-root": {
    padding: "38px 41px 38px 30px",
  },
}));

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props;

  return (
    <div className="px-[30px] py-[24px]">
      <p>{children}</p>
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </div>
  );
}

BootstrapDialogTitle.propTypes = {
  children: PropTypes.node,
  onClose: PropTypes.func.isRequired,
};

function List() {
  const { id } = useParams();

  const [data, setData] = useState({});
  const [dataList, setDataList] = useState([]);
  const [error, setError] = useState("");
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const getActivityById = setInterval(async () => {
      setLoaded(true);
      try {
        const { data: response } = await axios.get(
          `https://todo.api.devcode.gethired.id/activity-groups/${id}`
        );
        setData(response);
        setDataList(response.todo_items);
      } catch (error) {
        setError(error.message);
      }
      setLoaded(false);
    }, 1000);

    return () => clearInterval(getActivityById);
  }, []);

  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [title, setTitle] = useState("");
  const [priority, setPriority] = useState("");
  const createList = () => {
    axios
      .post("https://todo.api.devcode.gethired.id/todo-items", {
        activity_group_id: id,
        title: title,
        priority: priority,
      })
      .then(() => window.location.reload())
      .catch((error) => {
        console.log(error);
      });
  };

  const [anchorEl, setAnchorEl] = useState(null);
  const openMenu = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const [selectedOption, setSelectedOption] = useState("");
  const [sortedData, SetSortedData] = useState([]);

  const sorted = (data) => {
    if (data === "Z-A") {
      SetSortedData(
        [...dataList].sort((a, b) => b.title.localeCompare(a.title))
      );
    } else if (data === "A-Z") {
      SetSortedData(
        [...dataList].sort((a, b) => a.title.localeCompare(b.title))
      );
    } else if (data === "terbaru") {
      SetSortedData([...dataList].sort((a, b) => a.id - b.id));
    } else if (data === "terlama") {
      SetSortedData([...dataList].sort((a, b) => b.id - a.id));
    }
  };

  const [idDelete, setIdDelete] = useState("");
  const getId = (data) => {
    setIdDelete(data);
  };
  const [titleDelete, setTitleDelete] = useState("");
  const getTitle = (data) => {
    setTitleDelete(data);
  };

  const [openDelete, setOpenDelete] = useState(false);
  const handleClickOpenDelete = () => {
    setOpenDelete(true);
  };
  const handleCloseDelete = () => {
    setOpenDelete(false);
  };
  const handleDelete = (data) => {
    axios
      .delete(`https://todo.api.devcode.gethired.id/todo-items/${data}`)
      .then(console.log("berhasil"))
      .catch((error) => {
        console.log(error);
      });
  };

  const [titleActivity, setTitleActivity] = useState("");
  const [updateActivity, setUpdateActivity] = useState(false);
  const handleUpdate = () => {
    axios
      .put(`https://todo.api.devcode.gethired.id/activity-groups/${id}`, {
        title: titleActivity,
        created_at: "2023-04-14 12:00:00",
        updated_at: "2023-04-14 12:00:00",
      })
      .then(setUpdateActivity(false))
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Navbar />
      <div className="w-full py-[43px] px-[220px]">
        <div className="flex justify-between mb-[97px]">
          <div className="flex items-center gap-x-[19px]">
            <Link to="/">
              <svg
                width="32"
                height="32"
                viewBox="0 0 32 32"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M6.66675 16L14.6667 24"
                  stroke="#111111"
                  stroke-width="5"
                  stroke-linecap="square"
                />
                <path
                  d="M6.66675 16L14.6667 8"
                  stroke="#111111"
                  stroke-width="5"
                  stroke-linecap="square"
                />
              </svg>
            </Link>

            {updateActivity === false ? (
              <>
                <p className="text-4xl font-bold">{data.title}</p>
              </>
            ) : (
              <>
                <input
                  className="text-4xl font-bold border-b border-gray-500 bg-transparent !focus:border-b"
                  type="text"
                  value={titleActivity}
                  onChange={(e) => setTitleActivity(e.target.value)}
                  onBlur={handleUpdate}
                />
              </>
            )}

            <svg
              onClick={() => setUpdateActivity(true)}
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4 19.9998H8L18.5 9.49981C19.0304 8.96938 19.3284 8.24996 19.3284 7.49981C19.3284 6.74967 19.0304 6.03025 18.5 5.49981C17.9696 4.96938 17.2501 4.67139 16.5 4.67139C15.7499 4.67139 15.0304 4.96938 14.5 5.49981L4 15.9998V19.9998Z"
                stroke="#A4A4A4"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M13.5 6.49982L17.5 10.4998"
                stroke="#A4A4A4"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
          <div className="flex gap-[18px]">
            <div>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={openMenu}
                onClose={handleCloseMenu}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}>
                <MenuItem
                  onClick={() => {
                    setSelectedOption("terbaru");
                    sorted("terbaru");
                    handleCloseMenu();
                  }}
                  className="flex gap-[15px]">
                  <svg
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M3 4.5H9.75"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M3 9H8.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M3 13.5H8.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M11.25 11.25L13.5 13.5L15.75 11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M13.5 4.5V13.5"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                  <p>Terbaru</p>
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    setSelectedOption("terlama");
                    sorted("terlama");
                    handleCloseMenu();
                  }}
                  className="flex gap-[15px]">
                  <svg
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M3 4.5H8.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M3 9H8.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M3 13.5H9.75"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M11.25 6.75L13.5 4.5L15.75 6.75"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M13.5 4.5V13.5"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                  <p>Terlama</p>
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    setSelectedOption("A-Z");
                    sorted("A-Z");
                    handleCloseMenu();
                  }}
                  className="flex gap-[15px]">
                  <svg
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M11.25 7.5V3.75C11.25 2.715 11.715 2.25 12.75 2.25C13.785 2.25 14.25 2.715 14.25 3.75V7.5M14.25 5.25H11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M14.25 15.75H11.25L14.25 10.5H11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M3 11.25L5.25 13.5L7.5 11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M5.25 4.5V13.5"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                  <p>A-Z</p>
                </MenuItem>
                <MenuItem
                  onClick={() => {
                    setSelectedOption("Z-A");
                    sorted("Z-A");
                    handleCloseMenu();
                  }}
                  className="flex gap-[15px]">
                  <svg
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M11.25 15.75V12C11.25 10.965 11.715 10.5 12.75 10.5C13.785 10.5 14.25 10.965 14.25 12V15.75M14.25 13.5H11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M14.25 7.5H11.25L14.25 2.25H11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M3 11.25L5.25 13.5L7.5 11.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M5.25 4.5V13.5"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                  <p>Z-A</p>
                </MenuItem>
                <MenuItem className="flex gap-[15px]">
                  <svg
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M2.25 6.75L5.25 3.75M5.25 3.75L8.25 6.75M5.25 3.75V14.25"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M15.75 11.25L12.75 14.25M12.75 14.25L9.75 11.25M12.75 14.25V3.75"
                      stroke="#16ABF8"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                  <p>Belum Selesai</p>
                </MenuItem>
              </Menu>
              <svg
                id="basic-button"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
                className="cursor-pointer"
                width="54"
                height="54"
                viewBox="0 0 54 54"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M18 24L22 20M22 20L26 24M22 20V34"
                  stroke="#888888"
                  stroke-width="1.5"
                  stroke-linecap="square"
                />
                <path
                  d="M36 30L32 34M32 34L28 30M32 34V20"
                  stroke="#888888"
                  stroke-width="1.5"
                  stroke-linecap="square"
                />
                <rect
                  x="0.5"
                  y="0.5"
                  width="53"
                  height="53"
                  rx="26.5"
                  stroke="#E5E5E5"
                />
              </svg>
            </div>
            <button
              onClick={handleClickOpen}
              className="bg-main py-[12px] px-[24px] rounded-[45px] flex gap-[6px] text-white text-lg font-semibold items-center">
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M12 4.99988V18.9999"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="square"
                  stroke-linejoin="round"
                />
                <path
                  d="M5 12H19"
                  stroke="white"
                  stroke-width="2"
                  stroke-linecap="square"
                  stroke-linejoin="round"
                />
              </svg>
              Tambah
            </button>
          </div>
        </div>
        {dataList < 1 || dataList === [] ? (
          <EmptyList />
        ) : (
          <div>
            {selectedOption === "" ? (
              <>
                {dataList.map((item) => (
                  <ListItem
                    item={item}
                    getId={getId}
                    getTitle={getTitle}
                    modal={handleClickOpenDelete}
                    idParent={id}
                    key={item.id}
                  />
                ))}
              </>
            ) : (
              <>
                {sortedData.map((item) => (
                  <ListItem
                    item={item}
                    getId={getId}
                    getTitle={getTitle}
                    modal={handleClickOpenDelete}
                    idParent={id}
                    key={item.id}
                  />
                ))}
              </>
            )}
          </div>
        )}
      </div>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        className="rounded-[12px]">
        <BootstrapDialogTitle
          id="customized-dialog-title"
          onClose={handleClose}>
          Modal title
        </BootstrapDialogTitle>
        <DialogContent dividers>
          <div className="mb-[26px]">
            <label
              for="last_name"
              className="block mb-2 text-xs font-semibold text-black">
              NAMA LIST ITEM
            </label>
            <input
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className="bg-white border border-boldwhite text-boldgrey text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
              placeholder="Tambahkan nama list item"
              required
            />
          </div>
          <div>
            <label class="block mb-2 text-xs font-semibold text-black">
              PRIORITY
            </label>
            <select
              value={priority}
              onChange={(e) => setPriority(e.target.value)}
              class="bg-white border border-boldwhite text-boldgrey text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[205px] p-2.5">
              <option selected value="very-high">
                <div className="w-[14px] h-[14px] rounded-full bg-red"></div>
                <p>Very High</p>
              </option>
              <option value="high">
                <div className="w-[14px] h-[14px] rounded-full bg-yellow"></div>
                <p>High</p>
              </option>
              <option value="normal">
                <div className="w-[14px] h-[14px] rounded-full bg-green"></div>
                <p>Medium</p>
              </option>
              <option value="low">
                <div className="w-[14px] h-[14px] rounded-full bg-blue"></div>
                <p>Low</p>
              </option>
              <option value="very-low">
                <div className="w-[14px] h-[14px] rounded-full bg-purple"></div>
                <p>Very Low</p>
              </option>
            </select>
          </div>
        </DialogContent>
        <DialogActions className="!pt-[15px] !pr-[40px] !pb-[19px]">
          <button
            onClick={createList}
            className="bg-main py-[12px] px-[39px] rounded-[45px] text-white text-lg font-semibold items-center">
            Simpan
          </button>
        </DialogActions>
      </BootstrapDialog>
      <Dialog open={openDelete} onClose={handleCloseDelete}>
        <DialogContent className="w-[490px] bg-white rounded-[12px]">
          <div className="flex justify-center p-[16px] w-full">
            <svg
              width="84"
              height="84"
              viewBox="0 0 84 84"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                d="M42 52.5V52.535M42 31.5V38.5V31.5Z"
                stroke="#ED4C5C"
                stroke-width="4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M17.5002 66.5012H66.5002C67.6423 66.4932 68.765 66.2059 69.7705 65.6643C70.7761 65.1227 71.6338 64.3433 72.2689 63.3941C72.904 62.4449 73.2972 61.3546 73.4142 60.2186C73.5312 59.0825 73.3685 57.935 72.9402 56.8762L48.0902 14.0012C47.4848 12.9071 46.5975 11.9952 45.5203 11.3601C44.4432 10.725 43.2156 10.3901 41.9652 10.3901C40.7148 10.3901 39.4872 10.725 38.41 11.3601C37.3329 11.9952 36.4455 12.9071 35.8402 14.0012L10.9902 56.8762C10.57 57.9108 10.4033 59.0308 10.5042 60.1428C10.6051 61.2549 10.9705 62.3266 11.57 63.2687C12.1694 64.2107 12.9856 64.9956 13.9502 65.558C14.9149 66.1203 16.0001 66.4438 17.1152 66.5012"
                stroke="#ED4C5C"
                stroke-width="4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
          <div className="w-full p-[16px] mb-[16px]">
            <p className="text-black text-lg font-medium text-center">
              Apakah anda yakin menghapus item{" "}
              <span className="font-bold">"{titleDelete}"</span>?
            </p>
          </div>
        </DialogContent>
        <div className="w-full flex justify-center gap-[20px] p-[16px] mb-[20px]">
          <button
            onClick={handleCloseDelete}
            className="bg-softgrey py-[12px] px-[51px] rounded-[45px] flex gap-[6px] text-boldgrey text-lg font-semibold items-center">
            Batal
          </button>
          <button
            onClick={() => {
              handleDelete(idDelete);
              handleCloseDelete();
            }}
            className="bg-red py-[12px] px-[45px] rounded-[45px] flex gap-[6px] text-white text-lg font-semibold items-center">
            Hapus
          </button>
        </div>
      </Dialog>
    </div>
  );
}

export default List;
