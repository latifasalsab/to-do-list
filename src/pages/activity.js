import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import Navbar from "../components/navbar";
import EmptyState from "../components/emptyState";
import ActivityState from "../components/activityState";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";

function Activity() {
  const [data, setData] = useState([]);
  const [error, setError] = useState("");
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const getAllActivity = setInterval(async () => {
      setLoaded(true);
      try {
        const { data: response } = await axios.get(
          "https://todo.api.devcode.gethired.id/activity-groups?email=latifasalsa.works@gmail.com"
        );
        setData(response.data);
      } catch (error) {
        setError(error.message);
      }
      setLoaded(false);
    }, 1000);

    return () => clearInterval(getAllActivity);
  }, []);

  const [open, setOpen] = useState(false);
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const [id, setId] = useState("");
  const getId = (data) => {
    setId(data);
  };

  const [title, setTitle] = useState("");
  const getTitle = (data) => {
    setTitle(data);
  };

  const handleDelete = (data) => {
    axios
      .delete(`https://todo.api.devcode.gethired.id/activity-groups/${data}`)
      .then(setOpenAlert(true))
      .catch((error) => {
        console.log(error);
      });
  };

  const [openAlert, setOpenAlert] = useState(false);
  const handleCloseAlert = () => {
    setOpenAlert(false);
  };

  const createActivity = () => {
    axios
      .post("https://todo.api.devcode.gethired.id/activity-groups", {
        title: "New Activity",
        email: "latifasalsa.works@gmail.com",
      })
      .then(console.log("bisa"))
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Navbar />
      <div className="w-full py-[43px] px-[220px]">
        <div className="flex justify-between mb-[65px]">
          <p className="text-4xl font-bold text-black">Activity</p>
          <button
            onClick={createActivity}
            className="bg-main py-[12px] px-[24px] rounded-[45px] flex gap-[6px] text-white text-lg font-semibold items-center">
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12 4.99988V18.9999"
                stroke="white"
                stroke-width="2"
                stroke-linecap="square"
                stroke-linejoin="round"
              />
              <path
                d="M5 12H19"
                stroke="white"
                stroke-width="2"
                stroke-linecap="square"
                stroke-linejoin="round"
              />
            </svg>
            Tambah
          </button>
        </div>
        {data === [] || data === "" || data === undefined || data.length < 1 ? (
          <EmptyState />
        ) : (
          <div
            className="flex flex-wrap gap-x-5 gap-y-[26px]">
            {data.map((item) => (
              <ActivityState
                modal={handleClickOpen}
                getId={getId}
                getTitle={getTitle}
                item={item}
                key={item.id}
              />
            ))}
          </div>
        )}
      </div>
      <Dialog open={open} onClose={handleClose}>
        <DialogContent className="w-[490px] bg-white rounded-[12px]">
          <div className="flex justify-center p-[16px] w-full">
            <svg
              width="84"
              height="84"
              viewBox="0 0 84 84"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                d="M42 52.5V52.535M42 31.5V38.5V31.5Z"
                stroke="#ED4C5C"
                stroke-width="4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M17.5002 66.5012H66.5002C67.6423 66.4932 68.765 66.2059 69.7705 65.6643C70.7761 65.1227 71.6338 64.3433 72.2689 63.3941C72.904 62.4449 73.2972 61.3546 73.4142 60.2186C73.5312 59.0825 73.3685 57.935 72.9402 56.8762L48.0902 14.0012C47.4848 12.9071 46.5975 11.9952 45.5203 11.3601C44.4432 10.725 43.2156 10.3901 41.9652 10.3901C40.7148 10.3901 39.4872 10.725 38.41 11.3601C37.3329 11.9952 36.4455 12.9071 35.8402 14.0012L10.9902 56.8762C10.57 57.9108 10.4033 59.0308 10.5042 60.1428C10.6051 61.2549 10.9705 62.3266 11.57 63.2687C12.1694 64.2107 12.9856 64.9956 13.9502 65.558C14.9149 66.1203 16.0001 66.4438 17.1152 66.5012"
                stroke="#ED4C5C"
                stroke-width="4"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </div>
          <div className="w-full p-[16px] mb-[16px]">
            <p className="text-black text-lg font-medium text-center">
              Apakah anda yakin menghapus activity{" "}
              <span className="font-bold">"{title}"</span>?
            </p>
          </div>
        </DialogContent>
        <div className="w-full flex justify-center gap-[20px] p-[16px] mb-[20px]">
          <button
            onClick={handleClose}
            className="bg-softgrey py-[12px] px-[51px] rounded-[45px] flex gap-[6px] text-boldgrey text-lg font-semibold items-center">
            Batal
          </button>
          <button
            onClick={() => {
              handleDelete(id);
              handleClose();
            }}
            className="bg-red py-[12px] px-[45px] rounded-[45px] flex gap-[6px] text-white text-lg font-semibold items-center">
            Hapus
          </button>
        </div>
      </Dialog>
      <Dialog open={openAlert} onClose={handleCloseAlert}>
        <DialogContent
          onClose={handleCloseAlert}
          className="w-[490px] bg-white rounded-[12px]">
          <div className="flex gap-[10px] items-center">
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                d="M12 21C16.9706 21 21 16.9706 21 12C21 7.02944 16.9706 3 12 3C7.02944 3 3 7.02944 3 12C3 16.9706 7.02944 21 12 21Z"
                stroke="#00A790"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M12 8V12"
                stroke="#00A790"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M12 16H12.01"
                stroke="#00A790"
                stroke-width="2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            <p className="text-black text-sm font-medium">
              Activity berhasil dihapus
            </p>
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default Activity;
