import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Activity from "./pages/activity";
import List from "./pages/list";
import "./App.css";

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={<Activity />} />
          <Route path="/activity" element={<Activity />} />
          <Route path="/detail/:id" element={<List />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
